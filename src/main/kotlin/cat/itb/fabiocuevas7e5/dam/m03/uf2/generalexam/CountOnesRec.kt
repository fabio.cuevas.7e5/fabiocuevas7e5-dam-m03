package cat.itb.fabiocuevas7e5.dam.m03.uf2.generalexam

import java.util.*

fun main(){
    val sc = Scanner(System.`in`)
    val userInput = sc.nextInt()
    println(CountOnesRec(userInput))
}

fun CountOnesRec(userInput: Int): Int{
    var counter = 0
    if(userInput == 0) {
        return 0
    }
    if(userInput%10 == 1) {
        counter++
    }
    return CountOnesRec(userInput/10) + counter
}