package cat.itb.fabiocuevas7e5.dam.m03.uf2.generalexam

data class OilSpill(val nomSpill: String,
                 val nombreEmp: String,
                 val litrosSpill: Int,
                 val toxicitat: Double,
                 val gravedad: Double) {
}