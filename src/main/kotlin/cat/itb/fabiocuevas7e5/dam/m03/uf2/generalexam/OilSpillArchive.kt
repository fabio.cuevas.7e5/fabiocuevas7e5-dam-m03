package cat.itb.fabiocuevas7e5.dam.m03.uf2.generalexam

import cat.itb.fabiocuevas7e5.dam.m03.uf2.functions.max
import cat.itb.fabiocuevas7e5.dam.m03.uf2.functions.sum
import java.util.*

data class OilSpillArchive(val scanner: Scanner, val vessaments: MutableList<OilSpill> = mutableListOf()) {

    fun vessaments() {
        val nomSpill = scanner.next()
        val nombreEmp = scanner.next()
        val litrosSpill = scanner.nextInt()
        val toxicitat = scanner.nextDouble()
        val gravedad = litrosSpill * toxicitat
        scanner.nextLine()
        vessaments.add(OilSpill(nomSpill, nombreEmp, litrosSpill, toxicitat, gravedad))
    }

    fun showVessaments() {
        println("name - company - litters - toxicity - gravity")
        println(vessaments)
    }


    fun searchSpillLit(nomSpill: String): List<Int> {
        val listVessament = mutableListOf<Int>()
        for (i in vessaments) {
            val posicion = i.nomSpill
            if (posicion == nomSpill) {
                listVessament.add(i.litrosSpill)
            } else {
                println("Can't find oil spill")
            }
        }
        return listVessament
    }

    fun showSpillLitters(scanner: Scanner) {
        val vessament = scanner.nextLine()
        println(searchSpillLit(vessament))
    }

    fun searchSpillGra(nomSpill: String): List<Double> {
        val listVessament = mutableListOf<Double>()
        for (i in vessaments) {
            val posicion = i.nomSpill
            if (posicion == nomSpill) {
                listVessament.add(i.gravedad)
            } else {
                println("Can't find oil spill")
            }
        }
        return listVessament
    }

    fun showSpillGravity(scanner: Scanner) {
        val vessament = scanner.nextLine()
        println(searchSpillGra(vessament))
    }

    fun searchCompany(nomCompany: String): List<OilSpill> {
        val listVessament = mutableListOf<OilSpill>()
        for (i in vessaments) {
            val posicion = i.nombreEmp
            if (posicion == nomCompany) {
                listVessament.add(i)
            } else {
                println("Can't find oil spill")
            }
        }
        return listVessament
    }

    fun showSpillsForCompany(scanner: Scanner) {
        val vessament = scanner.nextLine()
        println("name - company - litters - toxicity - gravity")
        println(searchCompany(vessament))
    }


    fun worstSpill(){
        val firstSpill = vessaments[0]
        for(i in vessaments){
            if (i.gravedad > firstSpill.gravedad)
                println(i)
        }
        }





}