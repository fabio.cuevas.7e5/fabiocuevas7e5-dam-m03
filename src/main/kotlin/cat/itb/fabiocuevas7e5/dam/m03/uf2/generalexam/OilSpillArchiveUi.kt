package cat.itb.fabiocuevas7e5.dam.m03.uf2.generalexam

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val oilSpillArchiveUI= OilSpillArchiveUI(scanner)
    oilSpillArchiveUI.executarMenu(scanner)

}

class OilSpillArchiveUI(val scanner: Scanner) {
    val oilSpillArchive = OilSpillArchive(scanner)

    fun showMainMenu(){
        println("Chose option:\n" +
                "1. Add oil spill\n" +
                "2. List oil spills\n" +
                "3. Worst oil spill\n" +
                "4. Show Spill litters\n" +
                "5. Show spill gravity\n" +
                "6. Spills for company\n" +
                "7. Worst company\n" +
                "0. Exit")
    }
    fun executarMenu(scanner: Scanner){
        var seleccion = -1
        while(seleccion != 0){
            showMainMenu()
            val seleccion = scanner.nextInt()
            scanner.nextLine()
            when(seleccion){
                1-> oilSpillArchive.vessaments()
                2-> oilSpillArchive.showVessaments()
                3-> oilSpillArchive.worstSpill()
                4-> oilSpillArchive.showSpillLitters(scanner)
                5-> oilSpillArchive.showSpillGravity(scanner)
                6->oilSpillArchive.showSpillsForCompany(scanner)

            }
        }
    }
}