package cat.itb.fabiocuevas7e5.dam.m03.uf2.recursivity

import cat.itb.fabiocuevas7e5.dam.m03.uf1.data.project.scanner
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    puntos(scanner.nextInt())

}

fun puntos(a: Int){
    if(a == 0)
        return
    print(".")
    puntos(a -1)
}