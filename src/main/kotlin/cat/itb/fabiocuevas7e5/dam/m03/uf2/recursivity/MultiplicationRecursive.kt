package cat.itb.fabiocuevas7e5.dam.m03.uf2.recursivity

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println(multiplicationRecursive(scanner.nextInt(), scanner.nextInt()))
}

fun multiplicationRecursive(a: Int,b: Int): Int{
    if(b == 0)
        return 0
    else
        return a + multiplicationRecursive(a, b-1)
}