package cat.itb.fabiocuevas7e5.dam.m03.uf2.recursivity


fun main(){
    imprimirCançoPomes(10)
}
fun getAppleSongStanza(pomes: Int){
    println("$pomes té el pomer,\n" +
            "de $pomes una, de N una,\n" +
            "$pomes té el pomer,\n" +
            "de $pomes una en caigué.\n" +
            "\n" +
            "Si mireu el vent d'on vé\n" +
            "veureu el pomer com dansa,\n" +
            "si mireu el vent d'on vé\n" +
            "veureu com dansa el pomer.")
}
fun imprimirCançoPomes(pomes : Int){
    if(pomes==0)
        return
    getAppleSongStanza(pomes)
    imprimirCançoPomes(pomes-1)
}
