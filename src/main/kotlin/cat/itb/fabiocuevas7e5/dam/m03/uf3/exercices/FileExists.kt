package cat.itb.fabiocuevas7e5.dam.m03.uf3.exercices
import java.util.Scanner
import kotlin.io.path.Path
import kotlin.io.path.exists

fun main(){
    val sc = Scanner(System.`in`)
    val filePath = sc.nextLine()
    var path = Path(filePath)
    var fileExists = path.exists()

    if(fileExists){
        print("true")
    } else {
        print("false")
    }


}

