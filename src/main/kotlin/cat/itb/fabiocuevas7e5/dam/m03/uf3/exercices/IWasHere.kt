package cat.itb.fabiocuevas7e5.dam.m03.uf3.exercices

import java.io.File
import java.util.*
import java.time.LocalDateTime

fun main(){
    var content = "${LocalDateTime.now()}\n"
    var textFile = "i_was_here.txt"
    File(textFile).appendText("I was Here: $content")
}