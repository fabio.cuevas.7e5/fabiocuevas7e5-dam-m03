package cat.itb.fabiocuevas7e5.dam.m03.uf3.exercices

import java.io.File
import java.nio.file.Path
import kotlin.io.path.*


fun main() {
    val path = Path("src/main/kotlin/cat/itb/marcvives/daw/m03/uf3")
    val files1 : List<Path> = path.listDirectoryEntries()
    val kotlinFiles : List<Path> = path.listDirectoryEntries("*.kt")
    kotlinFiles.forEach{ ktFile -> println(ktFile)}


    println("-----------------------------------------------")

    path.toFile().walk().forEach{ file ->
        println(file)
    }

    println("-----------------------------------------------")

    val files2 : List<File> =  path.toFile().walk().toList()
    for (file in files2){
        println(file)
    }
}
