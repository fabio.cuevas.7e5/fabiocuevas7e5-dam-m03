package cat.itb.fabiocuevas7e5.dam.m03.uf3.exercices

import java.time.LocalDateTime
import kotlin.io.path.Path
import kotlin.io.path.copyTo
import kotlin.io.path.createDirectories
import kotlin.io.path.exists

fun main() {
    val dataText = LocalDateTime.now().toString()
    val rutaHome = System.getProperty("user.home")
    //val separador = System.getProperty("file.separator")

    val pathHome = Path(rutaHome)
    val pathBackup = pathHome.resolve("backup").resolve(dataText)
    pathBackup.createDirectories()

    val pathProfile = pathHome.resolve(".profile")
    println("pathBackup: "+pathBackup)
    val pathBackupProfile = pathBackup.resolve(".profile")
    println(pathBackupProfile)
    pathProfile.copyTo(pathBackupProfile)

    println("Backup creat: "+pathBackupProfile.exists())
}
