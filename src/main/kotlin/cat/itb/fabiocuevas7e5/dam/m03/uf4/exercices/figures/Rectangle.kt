package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.figures

class Rectangle(val color: String, val height: Int, val width: Int) {

    fun paint(){
        for (i in 1..height){
            for (j in 1..width){
                print("${color}X")
            }
            println()
        }
        println()
    }
}