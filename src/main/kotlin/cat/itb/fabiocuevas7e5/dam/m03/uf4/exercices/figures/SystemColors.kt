package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.figures



class SystemColors {

    // --- Colors for system out ---
// Reset
    val RESET = "\u001b[0m" // Text Reset

// Regular Colors
    val BLACK = "\u001b[0;30m" // BLACK
    val RED = "\u001b[0;31m" // RED
    val GREEN = "\u001b[0;32m" // GREEN
    val YELLOW = "\u001b[0;33m" // YELLOW
    val BLUE = "\u001b[0;34m" // BLUE
    val PURPLE = "\u001b[0;35m" // PURPLE
    val CYAN = "\u001b[0;36m" // CYAN
    val WHITE = "\u001b[0;37m" // WHITE
}