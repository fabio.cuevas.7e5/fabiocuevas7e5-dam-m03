package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.figures

fun main(){

    val RED = "\u001b[0;31m"
    val GREEN = "\u001b[0;32m"
    val YELLOW = "\u001b[0;33m"
    val r1 = Rectangle(RED,3, 5)
    val r2 = Rectangle(YELLOW,2,2 )
    val r3 = Rectangle(GREEN,3, 5)

    r1.paint()
    r2.paint()
    r3.paint()
}