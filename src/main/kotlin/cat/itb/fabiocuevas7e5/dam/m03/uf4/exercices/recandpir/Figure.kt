package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.recandpir

open class Figure (val color: String,
                   val height: Int){

}
class Rectangle(color: String, height: Int, val width: Int) : Figure(color, height){
    fun paint(){
        for (i in 1..height){
            for (j in 1..width){
                print("${color}X")
            }
            println()
        }
        println()
    }
}
    class Piramid(color: String, height: Int) : Figure(color, height){
        fun paintPir(){
            for (i in 1..height){
                for (j in 1..i){
                    print("${color}X")
                }
                println()
            }
            println()
        }
    }
