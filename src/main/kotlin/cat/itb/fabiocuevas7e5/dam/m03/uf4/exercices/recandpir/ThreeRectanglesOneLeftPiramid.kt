package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.recandpir



fun main(){
    val RED = "\u001b[0;31m"
    val GREEN = "\u001b[0;32m"
    val YELLOW = "\u001b[0;33m"

    val r1 = Rectangle(RED, 4,5)
    val p1 = Piramid(YELLOW, 3)
    val r2 = Rectangle(GREEN, 3,5)

    r1.paint()
    p1.paintPir()
    r2.paint()
}