package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.shop

fun main(){
    val brand = BycicleBrand("Specialized", "USA")

    val b1 = BycicleModel("Jett 24", 5, brand)
    val b2 = BycicleModel("Hotwalk", 7, brand)


    b1.imprimir()
    b2.imprimir()
}