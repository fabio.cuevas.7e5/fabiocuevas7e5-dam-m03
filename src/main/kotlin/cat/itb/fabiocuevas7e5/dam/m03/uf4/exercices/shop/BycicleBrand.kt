package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.shop

class BycicleBrand(val name: String, val country: String) {
    override fun toString(): String {
        return "BycicleBrand{name='$name', country='$country'}"
    }
}