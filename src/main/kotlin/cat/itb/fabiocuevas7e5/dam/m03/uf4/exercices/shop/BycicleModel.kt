package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.shop


class BycicleModel(val name: String, val gears: Int, val brand: BycicleBrand){
    fun imprimir(){
        println(toString())
    }

    override fun toString(): String {
        return "BycicleModel{name='$name', gears=$gears, brand=$brand"
    }
}