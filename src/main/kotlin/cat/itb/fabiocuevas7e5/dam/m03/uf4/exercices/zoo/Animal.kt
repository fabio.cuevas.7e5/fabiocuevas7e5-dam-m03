package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.zoo

open class Animal(val name: String, val birthDate: Int)
{
    fun eat(){
        println("come")
    }
}