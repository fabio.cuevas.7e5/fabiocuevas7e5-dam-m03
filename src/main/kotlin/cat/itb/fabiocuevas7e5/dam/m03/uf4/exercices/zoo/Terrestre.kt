package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.zoo

class Terrestre(name: String, birthDate: Int) : Animal(name, birthDate){
    fun walk(){
        println("camina")
    }
}