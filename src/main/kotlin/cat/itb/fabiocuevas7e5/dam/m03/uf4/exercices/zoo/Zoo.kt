package cat.itb.fabiocuevas7e5.dam.m03.uf4.exercices.zoo

class Zoo(val listAnimal: MutableList<Animal>)
fun main(){
    val zoo = Zoo(mutableListOf(Terrestre("Caballo", 1999),
    Terrestre("Leon", 1990),
    Acuatic("Ballena", 2003),
    Acuatic("Tiburón",2005)
    ))

    zoo.listAnimal.forEach(){
        println(it.name)
        it.eat()
        when (it) {
            is Terrestre -> it.walk()
            is Acuatic -> it.swim()
        }

        if (it is Acuatic){
            it.swim()
        }
        if(it is Terrestre){
            it.walk()
        }
        }
    }
