package cat.itb.fabiocuevas7e5.dam.m03.uf6.beachapp

import cat.itb.fabiocuevas7e5.dam.m03.uf6.beachapp.ui.BeachApp
import cat.itb.fabiocuevas7e5.dam.m03.uf6.beachapp.data.BeachesDatabase
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    val database = BeachesDatabase()
    BeachApp(scanner, database).start()
}