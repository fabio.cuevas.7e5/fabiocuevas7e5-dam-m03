package cat.itb.fabiocuevas7e5.dam.m03.uf6.beachapp.data

import java.sql.Connection
import java.sql.DriverManager

val databaseFile = "sample.db"
class BeachesDatabase {
    var connection: Connection? = null

    fun connect(): Connection {
        connection = DriverManager.getConnection("jdbc:sqlite:$databaseFile")
        return connection!!
    }
}
