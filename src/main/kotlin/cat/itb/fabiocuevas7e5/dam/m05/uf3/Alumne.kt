package cat.itb.fabiocuevas7e5.dam.m05.uf3

import java.util.Date

data class Alumne(val nom: String,
                  val dataNaixement: Date,
                  val cognoms: String) {
    fun edat(): Int{
        return 0
    }
    fun nomComplet(): String{
        return "$nom $cognoms"
    }
}